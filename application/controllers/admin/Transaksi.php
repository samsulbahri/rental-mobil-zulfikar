<?php

class Transaksi extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('loged_in')) {
            redirect('auth/login');
        }
    }

    public function index()
    {
        $data['transaksi'] = $this->db->query('SELECT * FROM transaksi tr, mobil mb, customer cs WHERE tr.id_mobil = mb.id_mobil AND tr.id_customer = cs.id_customer')->result();
        $this->load->view('template_admin/header');
        $this->load->view('template_admin/sidebar');
        $this->load->view('admin/data_transaksi', $data);
        $this->load->view('template_admin/footer');
    }

    public function pembayaran($id)
    {
        $data['pembayaran'] = $this->db->query("SELECT * FROM transaksi WHERE id_transaksi = '$id'")->result();
        $this->load->view('template_admin/header');
        $this->load->view('template_admin/sidebar');
        $this->load->view('admin/konfirmasi_pembayaran', $data);
        $this->load->view('template_admin/footer');
    }

    public function cek_pembayaran()
    {
        $id = $this->input->post('id_transaksi');
        $bukti_pembayaran = $this->input->post('bukti_pembayaran');

        $data = array('status_pembayaran' => $bukti_pembayaran);
        $where = array('id_transaksi' => $id);

        $this->rental_model->update_data('transaksi', $data, $where);
        redirect('admin/transaksi');
    }

    public function download_pembayaran($id)
    {
        $this->load->helper('download');
        $filePembayaran = $this->rental_model->download_pembayaran($id);
        $file = 'assets/upload/' . $filePembayaran['bukti_pembayaran'];
        force_download($file, NULL);
    }

    public function transaksi_selesai($id)
    {
        $where = array('id_rental' => $id);
        $data['transaksi'] = $this->db->query("SELECT * FROM transaksi WHERE id_transaksi = '$id'")->result();

        $this->load->view('template_admin/header');
        $this->load->view('template_admin/sidebar');
        $this->load->view('admin/transaksi_selesai', $data);
        $this->load->view('template_admin/footer');
    }

    public function transaksi_selesai_aksi()
    {
        $id = $this->input->post('id_transaksi');
        $tanggal_pengembalian = $this->input->post('tanggal_pengembalian');
        $status_rental = $this->input->post('status_rental');
        $status_pengembalian = $this->input->post('status_pengembalian');
        $tanggal_kembali = $this->input->post('tanggal_kembali');
        $denda = $this->input->post('denda');

        $x = strtotime($tanggal_pengembalian);
        $y = strtotime($tanggal_kembali);

        $jumlahHari = abs($x - $y) / (60 * 60 * 24);
        $totalDenda = $jumlahHari * $denda;

        $data = array(
            'tanggal_pengembalian' => $tanggal_pengembalian,
            'status_rental' => $status_rental,
            'status_pengembalian' => $status_pengembalian,
            'total_denda' => $totalDenda
        );

        $where = array(
            'id_transaksi' => $id,
        );

        $this->rental_model->update_data('transaksi', $data, $where);
        $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                  Transaksi berhasil diupdate!
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>');
        redirect('admin/transaksi');
    }

    public function batal_transaksi($id)
    {
        $where_transaksi = array('id_transaksi' => $id);
        $data_transaksi = $this->rental_model->get_where($where_transaksi, 'transaksi')->row();

        $where_mobil = array('id_mobil' => $data_transaksi->id_mobil);
        $data_mobil = array('status' => '1');

        $this->rental_model->update_data('mobil', $data_mobil, $where_mobil);
        $this->rental_model->delete_data($where_transaksi, 'transaksi');

        $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                  Transaksi Berhasil Dibatalkan!
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>');
        redirect('admin/transaksi');
    }
}
