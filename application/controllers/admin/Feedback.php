<?php

class Feedback extends CI_Controller
{

    public function index()
    {
        $data['feedback'] = $this->db->query('SELECT * FROM feedback fb, mobil mb, customer cs WHERE fb.id_mobil = mb.id_mobil AND fb.id_user = cs.id_customer')->result();
        $this->load->view('template_admin/header');
        $this->load->view('template_admin/sidebar');
        $this->load->view('admin/feedback', $data);
        $this->load->view('template_admin/footer');
    }
}
