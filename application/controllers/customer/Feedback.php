<?php

class Feedback extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('loged_in')) {
            redirect('auth/login');
        }
    }

    public function index()
    {
        $customer = $this->session->userdata('id_customer');
        $data['transaksi'] = $this->db->query("SELECT * FROM transaksi tr, mobil mb, customer cs WHERE tr.id_mobil = mb.id_mobil AND tr.id_customer = cs.id_customer AND cs.id_customer='$customer' ORDER BY tr.id_transaksi DESC")->result();
        $this->load->view('template_customer/header');
        $this->load->view('template_customer/sidebar');
        $this->load->view('customer/feedback', $data);
        $this->load->view('template_customer/chat');
        $this->load->view('template_customer/footer');
    }

    public function tambah_feedback()
    {
        $id_customer = $this->input->post('id_customer');
        list($id_mobil, $id_transaksi) = explode('|', $this->input->post('mobil'));
        $pesan = $this->input->post('pesan');

        $data = array(
            'id_user' => $id_customer,
            'id_mobil' => $id_mobil,
            'pesan' => $pesan
        );

        $this->rental_model->insert_data($data, 'feedback');

        $update = array(
            'status_feedback' => '1'
        );

        $where = array(
            'id_transaksi' => $id_transaksi
        );

        $this->rental_model->update_data('transaksi', $update, $where);

        redirect('customer/dashboard');
    }
}
