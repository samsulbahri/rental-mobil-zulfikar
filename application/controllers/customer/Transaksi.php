<?php

class Transaksi extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('loged_in')) {
            redirect('auth/login');
        }
    }

    public function index()
    {
        $customer = $this->session->userdata('id_customer');
        $data['transaksi'] = $this->db->query("SELECT * FROM transaksi tr, mobil mb, customer cs WHERE tr.id_mobil = mb.id_mobil AND tr.id_customer = cs.id_customer AND cs.id_customer='$customer' ORDER BY tr.id_transaksi DESC")->result();
        $this->load->view('template_customer/header');
        $this->load->view('template_customer/sidebar');
        $this->load->view('customer/transaksi', $data);
        $this->load->view('template_customer/chat');
        $this->load->view('template_customer/footer');
    }

    public function pembayaran($id)
    {
        $data['transaksi'] = $this->db->query("SELECT * FROM transaksi tr, mobil mb, customer cs WHERE tr.id_mobil = mb.id_mobil AND tr.id_customer = cs.id_customer AND tr.id_transaksi='$id' ORDER BY tr.id_transaksi DESC")->result();
        $this->load->view('template_customer/header');
        $this->load->view('template_customer/sidebar');
        $this->load->view('customer/pembayaran', $data);
        $this->load->view('template_customer/chat');
        $this->load->view('template_customer/footer');
    }

    public function pembayaran_aksi()
    {
        $id = $this->input->post('id_transaksi');
        $bukti_pembayaran = $_FILES['bukti_pembayaran']['name'];
        if ($bukti_pembayaran) {
            $config['upload_path'] = './assets/upload/';
            $config['allowed_types'] = 'pdf|jpg|jpeg|png|tiff';

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('bukti_pembayaran')) {
                echo $this->upload->display_errors();
            } else {
                $bukti_pembayaran = $this->upload->data('file_name');
                $this->db->set('bukti_pembayaran', $bukti_pembayaran);
            }
        }

        $data = array(
            'bukti_pembayaran' => $bukti_pembayaran
        );

        $where = array(
            'id_transaksi' => $id
        );

        $this->rental_model->update_data('transaksi', $data, $where);
        $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                  Bukti Pembayaran Anda berhasil Diupload!
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>');
        redirect('customer/transaksi');
    }

    public function cetak_invoice($id)
    {
        $data['transaksi'] = $this->db->query("SELECT * FROM transaksi tr, mobil mb, customer cs WHERE tr.id_mobil = mb.id_mobil AND tr.id_customer = cs.id_customer AND tr.id_transaksi='$id'")->result();
        $this->load->view('customer/cetak_invoice', $data);
    }

    public function batal_transaksi($id)
    {
        $where_transaksi = array('id_transaksi' => $id);
        $data_transaksi = $this->rental_model->get_where($where_transaksi, 'transaksi')->row();

        $where_mobil = array('id_mobil' => $data_transaksi->id_mobil);
        $data_mobil = array('status' => '1');

        $this->rental_model->update_data('mobil', $data_mobil, $where_mobil);
        $this->rental_model->delete_data($where_transaksi, 'transaksi');

        $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                  Transaksi Berhasil Dibatalkan!
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>');
        redirect('customer/transaksi');
    }
}
