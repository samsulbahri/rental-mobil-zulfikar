<body>
    <div id="app">
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
                        <div class="login-brand">
                            <img src="<?= base_url('assets/stisla/img/stisla-fill.svg'); ?>" alt="logo" width="100" class="shadow-light rounded-circle">
                        </div>

                        <div class="card card-primary">
                            <div class="card-header">
                                <h4>Register</h4>
                            </div>

                            <div class="card-body">
                                <form method="POST" action="<?= base_url('register/'); ?>">
                                    <div class="row">
                                        <div class="form-group col-6">
                                            <label for="nama">Nama</label>
                                            <input id="nama" type="text" class="form-control" name="nama" autofocus>
                                            <?= form_error('nama', '<div class="text-small text-danger">', '</div>'); ?>
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="username">Username</label>
                                            <input id="username" type="text" class="form-control" name="username">
                                        </div>
                                        <?= form_error('username', '<div class="text-small text-danger">', '</div>'); ?>
                                    </div>

                                    <div class="form-group">
                                        <label for="alamat">Alamat</label>
                                        <input id="alamat" type="text" class="form-control" name="alamat">
                                        <div class="invalid-feedback">
                                        </div>
                                        <?= form_error('alamat', '<div class="text-small text-danger">', '</div>'); ?>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-6">
                                            <label for="password" class="d-block">Gender</label>
                                            <select class="form-control" name="gender" id="gender">
                                                <option value="">-- Pilih Gender --</option>
                                                <option value="laki-laki">Laki - Laki</option>
                                                <option value="perempuan">Perempuan</option>
                                            </select>
                                            <?= form_error('gender', '<div class="text-small text-danger">', '</div>'); ?>
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="no_telepon" class="d-block">Nomor Telepon</label>
                                            <input id="no_telepon" type="text" class="form-control" name="no_telepon">
                                            <?= form_error('no_telepon', '<div class="text-small text-danger">', '</div>'); ?>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-6">
                                            <label>Nomor KTP</label>
                                            <input id="no_ktp" type="text" class="form-control" name="no_ktp">
                                            <?= form_error('no_ktp', '<div class="text-small text-danger">', '</div>'); ?>
                                        </div>
                                        <div class="form-group col-6">
                                            <label>Password</label>
                                            <input id="password" type="password" class="form-control" name="password">
                                            <?= form_error('password', '<div class="text-small text-danger">', '</div>'); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-lg btn-block">
                                            Register
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>