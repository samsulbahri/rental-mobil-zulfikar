<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Detail Mobil</h1>
        </div>

        <?php foreach ($detail as $d) { ?>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-5">
                            <img width="300" src="<?= base_url('assets/upload/' . $d->gambar); ?>">
                        </div>
                        <div class="col-md-7">
                            <table class="table">
                                <tr>
                                    <td>Type Model</td>
                                    <td><?php if ($d->kode_type == "SDN") {
                                            echo 'Sedan';
                                        } else if ($d->kode_type == 'HTB') {
                                            echo 'Hatchback';
                                        } else if ($d->kode_type == 'MPV') {
                                            echo 'Multi Purpose Vehicle';
                                        } else {
                                            echo '<span class="text-danger">Type Mobil belum terdaftar</span>';
                                        } ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Merk</td>
                                    <td><?= $d->merk; ?></td>
                                </tr>

                                <tr>
                                    <td>No. Plat</td>
                                    <td><?= $d->no_plat; ?></td>
                                </tr>

                                <tr>
                                    <td>Warna</td>
                                    <td><?= $d->warna; ?></td>
                                </tr>

                                <tr>
                                    <td>Tahun</td>
                                    <td><?= $d->tahun; ?></td>
                                </tr>

                                <tr>
                                    <td>Harga</td>
                                    <td>Rp. <?= number_format($d->harga, 0, ',', '.'); ?></td>
                                </tr>

                                <tr>
                                    <td>Denda</td>
                                    <td>Rp. <?= number_format($d->denda, 0, ',', '.'); ?></td>
                                </tr>

                                <tr>
                                    <td>Status</td>
                                    <td>
                                        <?php if ($d->status == "0") {
                                            echo '<span class="badge badge-danger">Tidak Tersedia</span>';
                                        } else {
                                            echo '<span class="badge badge-primary">Tersedia</span>';
                                        } ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Supir</td>
                                    <td>
                                        <?php if ($d->supir == "0") {
                                            echo '<span class="badge badge-danger">Tidak Tersedia</span>';
                                        } else {
                                            echo '<span class="badge badge-primary">Tersedia</span>';
                                        } ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td>MP3 Player</td>
                                    <td>
                                        <?php if ($d->mp3_player == "0") {
                                            echo '<span class="badge badge-danger">Tidak Tersedia</span>';
                                        } else {
                                            echo '<span class="badge badge-primary">Tersedia</span>';
                                        } ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Central Lock</td>
                                    <td>
                                        <?php if ($d->central_lock == "0") {
                                            echo '<span class="badge badge-danger">Tidak Tersedia</span>';
                                        } else {
                                            echo '<span class="badge badge-primary">Tersedia</span>';
                                        } ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td>AC</td>
                                    <td>
                                        <?php if ($d->ac == "0") {
                                            echo '<span class="badge badge-danger">Tidak Tersedia</span>';
                                        } else {
                                            echo '<span class="badge badge-primary">Tersedia</span>';
                                        } ?>
                                    </td>
                                </tr>
                            </table>

                            <a class="btn btn-sm btn-warning ml-4" href="<?= base_url('admin/data_mobil'); ?>">Kembali</a>

                            <a class="btn btn-sm btn-primary ml-4" href="<?= base_url('admin/data_mobil/update_mobil/' . $d->id_mobil); ?>">Update</a>

                            <a class="btn btn-sm btn-danger ml-4" href="<?= base_url('admin/data_mobil/delete_mobil/' . $d->id_mobil); ?>">Hapus</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </section>
</div>