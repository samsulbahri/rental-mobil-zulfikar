<div class="main-content">
    <div class="section">
        <div class="section-header">
            <h1>Transaksi Selesai</h1>
        </div>
    </div>

    <?php foreach ($transaksi as $t) { ?>
        <form action="<?= base_url('admin/transaksi/transaksi_selesai_aksi'); ?>" method="POST">
            <input type="hidden" name="id_transaksi" value="<?= $t->id_transaksi; ?>">
            <input type="hidden" name="tanggal_kembali" value="<?= $t->tanggal_kembali; ?>">
            <input type="hidden" name="denda" value="<?= $t->denda; ?>">

            <div class="form-group">
                <label>Tanggal Pengembalian</label>
                <input type="date" name="tanggal_pengembalian" class="form-control" value="<?= $t->tanggal_pengembalian; ?>">
            </div>
            <div class="form-group">
                <label>Status Pengembalian</label>
                <select name="status_pengembalian" class="form-control">
                    <option value="<?= $t->status_pengembalian; ?>"><?= $t->status_pengembalian; ?></option>
                    <option value="Kembali">Kembali</option>
                    <option value="Belum Kembali">Belum Kembali</option>
                </select>
            </div>
            <div class="form-group">
                <label>Status Rental</label>
                <select name="status_rental" class="form-control">
                    <option value="<?= $t->status_rental; ?>"><?= $t->status_rental; ?></option>
                    <option value="Selesai">Selesai</option>
                    <option value="Belum Selesai">Belum Selesai</option>
                </select>
            </div>

            <button type="submit" class="btn btn-success">Save</button>
        </form>
    <?php } ?>
</div>