      <!-- Main Content -->
      <div class="main-content">
          <div class="section">
              <div class="section-header">
                  <h1>Feedback</h1>
              </div>
          </div>
          <section class="section">
              <div class="row">
                  <?php foreach ($feedback as $f) { ?>
                      <div class="col-lg-6 col-md-6 col-sm-12">
                          <div class="card card-statistic-2">
                              <div class="card-header">
                                  <div class="card-stats-title font-weight-bold"><?= $f->nama; ?>
                                  </div>
                              </div>
                              <div class="card-body">
                                  <div class="font-weight-light" style="font-size: 15px;">
                                      <?= $f->pesan; ?>
                                  </div>
                              </div>
                              <div class="card-footer">
                                  <div class="card-icon shadow-primary bg-primary">
                                      <i class="fas fa-car"></i>
                                  </div>
                                  <div class="card-wrap">
                                      <div class="card-header mt-2">
                                          <h4><?= $f->no_plat; ?></h4>
                                      </div>
                                      <div class="card-body" style="font-size: 13px;">
                                          <?= $f->merk; ?>
                                      </div>
                                  </div>
                              </div>
                              <div class="card-stats">


                              </div>

                          </div>
                      </div>
                  <?php } ?>
              </div>
          </section>
      </div>