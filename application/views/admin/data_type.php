<div class="main-content">
    <div class="section">
        <div class="section-header">
            <h1>Data Type Mobil</h1>
        </div>
    </div>

    <a class="btn btn-primary mb-3" href="<?= base_url('admin/data_type/tambah_type'); ?>">Tambah Type</a>

    <?= $this->session->flashdata('pesan'); ?>

    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th width="20px">No</th>
                <th>Kode Type</th>
                <th>Nama Type</th>
                <th>Aksi</th>
            </tr>
        </thead>

        <tbody>
            <?php
            $no = 1;
            foreach ($type as $t) { ?>

                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $t->kode_type; ?></td>
                    <td><?= $t->nama_type; ?></td>
                    <td>
                        <a href="<?= base_url('admin/data_type/update_type/' . $t->id_type); ?>" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                        <a href="<?= base_url('admin/data_type/delete_type/' . $t->id_type); ?>" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></a>
                    </td>
                </tr>

            <?php } ?>
        </tbody>
    </table>
</div>