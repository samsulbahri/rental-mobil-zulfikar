<div class="main-content">
    <div class="section">
        <div class="section-header">
            <h1>Data Customer</h1>
        </div>
    </div>

    <a class="btn btn-primary mb-3" href="<?= base_url('admin/data_customer/tambah_customer'); ?>">Tambah Customer</a>

    <?= $this->session->flashdata('pesan'); ?>

    <table class="table table-striped table-responsive table-bordered">
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Username</th>
            <th>Alamat</th>
            <th>Gender</th>
            <th>No. Telepon</th>
            <th>No. KTP</th>
            <th>Password</th>
            <th>Action</th>
        </tr>

        <?php
        $no = 1;
        foreach ($customer as $c) { ?>
            <tr>
                <td><?= $no++; ?></td>
                <td><?= $c->nama; ?></td>
                <td><?= $c->username; ?></td>
                <td><?= $c->alamat; ?></td>
                <td><?= $c->gender; ?></td>
                <td><?= $c->no_telepon; ?></td>
                <td><?= $c->no_ktp; ?></td>
                <td><?= $c->password; ?></td>
                <td>
                    <a href="<?= base_url('admin/data_customer/delete_customer/' . $c->id_customer); ?>" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
                    <a href="<?= base_url('admin/data_customer/update_customer/' . $c->id_customer); ?>" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                </td>
            </tr>
        <?php } ?>
    </table>
</div>