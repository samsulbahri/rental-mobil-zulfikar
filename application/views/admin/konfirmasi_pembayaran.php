<div class="main-content">
    <div class="section">
        <div class="section-header">
            <h1>Konfirmasi Pembayaran</h1>
        </div>
        <center>
            <div class="card" style="width: 55%;">
                <div class="card-header">
                    Konfirmasi Pembayaran
                </div>

                <div class="card-body">
                    <form action="<?= base_url('admin/transaksi/cek_pembayaran'); ?>" method="POST">
                        <?php foreach ($pembayaran as $p) { ?>
                            <a href="<?= base_url('admin/transaksi/download_pembayaran/' . $p->id_transaksi); ?>" class="btn btn-sm btn-success"><i class="fas fa-download"></i> Download Bukti Pembayaran</a>
                            <input type="hidden" value="<?= $p->id_transaksi; ?>" name="id_transaksi">
                            <div class="custom-control custom-switch ml-5">
                                <input type="checkbox" class="custom-control-input" value="1" name="bukti_pembayaran" id="bukti_pembayaran">
                                <label class="custom-control-label" for="bukti_pembayaran">Konfirmasi Pembayaran</label>
                            </div>

                            <hr>

                            <button type="submit" class="btn btn-sm btn-primary">Simpan</button>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </center>
    </div>
</div>