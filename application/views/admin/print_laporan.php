<h3 class="text-center">Laporan Transaksi Rental Model</h3>
<table>
    <tr>
        <td>Dari Tanggal</td>
        <td>:</td>
        <td><?= date('d-M-Y', strtotime($_GET['dari'])); ?></td>
    </tr>

    <tr>
        <td>Sampai Tanggal</td>
        <td>:</td>
        <td><?= date('d-M-Y', strtotime($_GET['sampai'])); ?></td>
    </tr>
</table>

<table class="table-responsive table table-bordered table-striped mt-4">
    <tr>
        <th>No</th>
        <th>Customer</th>
        <th>Mobil</th>
        <th>Tgl. Rental</th>
        <th>Tgl. Kembali</th>
        <th>Harga/Hari</th>
        <th>Denda/Hari</th>
        <th>Total Denda</th>
        <th>Tgl. Dikembalikan</th>
        <th>Status Pengembalian</th>
        <th>Status Rental</th>
    </tr>
    <?php $no = 1;
    foreach ($laporan as $t) { ?>
        <tr>
            <td><?= $no++; ?></td>
            <td><?= $t->nama; ?></td>
            <td><?= $t->merk; ?></td>
            <td><?= date('d/m/Y', strtotime($t->tanggal_rental)); ?></td>
            <td><?= date('d/m/Y', strtotime($t->tanggal_kembali)); ?></td>
            <td><?= number_format($t->harga, 0, ',', '.'); ?></td>
            <td><?= number_format($t->denda, 0, ',', '.'); ?></td>
            <td><?= number_format($t->total_denda, 0, ',', '.'); ?></td>
            <td><?php if ($t->tanggal_pengembalian == '0000-00-00') {
                    echo '-';
                } else {
                    echo date('d/m/Y', strtotime($t->tanggal_pengembalian));
                } ?>
            </td>
            <td>
                <?= $t->status_pengembalian; ?>
            </td>
            <td>
                <?= $t->status_rental; ?>
            </td>
        </tr>
    <?php } ?>
</table>

<script>
    window.print();
</script>