<div class="main-content">
    <div class="section">
        <div class="section-header">
            <h1>Form Tambah Customer</h1>
        </div>
    </div>

    <form action="<?= base_url('admin/data_customer/tambah_customer_aksi'); ?>" method="POST">
        <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" class="form-control">
            <?= form_error('nama', '<div class="text-small text-danger">', '</div>'); ?>
        </div>
        <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control">
            <?= form_error('username', '<div class="text-small text-danger">', '</div>'); ?>
        </div>
        <div class="form-group">
            <label>Alamat</label>
            <input type="text" name="alamat" class="form-control">
            <?= form_error('alamat', '<div class="text-small text-danger">', '</div>'); ?>
        </div>
        <div class="form-group">
            <label>Gender</label>
            <select name="gender" class="form-control">
                <option value="">-- Pilih Gender --</option>
                <option value="laki-laki">laki - laki</option>
                <option value="perempuan">perempuan</option>
            </select>
            <?= form_error('gender', '<div class="text-small text-danger">', '</div>'); ?>
        </div>
        <div class="form-group">
            <label>No. Telepon</label>
            <input type="text" name="no_telepon" class="form-control">
            <?= form_error('no_telepon', '<div class="text-small text-danger">', '</div>'); ?>
        </div>
        <div class="form-group">
            <label>No. KTP</label>
            <input type="text" name="no_ktp" class="form-control">
            <?= form_error('no_ktp', '<div class="text-small text-danger">', '</div>'); ?>
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="text" name="password" class="form-control">
            <?= form_error('password', '<div class="text-small text-danger">', '</div>'); ?>
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
        <button type="reset" class="btn btn-danger">Reset</button>
    </form>
</div>