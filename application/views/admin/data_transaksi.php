<div class="main-content">
    <div class="section">
        <div class="section-header">
            <h1>Data Transaksi</h1>
        </div>

        <?= $this->session->flashdata('pesan'); ?>

        <table class="table-responsive table table-bordered table-striped">
            <tr>
                <th>No</th>
                <th>Customer</th>
                <th>Mobil</th>
                <th>Tgl. Rental</th>
                <th>Tgl. Kembali</th>
                <th>Harga/Hari</th>
                <th>Denda/Hari</th>
                <th>Total Denda</th>
                <th>Tgl. Dikembalikan</th>
                <th>Status Pengembalian</th>
                <th>Status Rental</th>
                <th>Cek Pembayaran</th>
                <th>Action</th>
            </tr>
            <?php $no = 1;
            foreach ($transaksi as $t) { ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $t->nama; ?></td>
                    <td><?= $t->merk; ?></td>
                    <td><?= date('d/m/Y', strtotime($t->tanggal_rental)); ?></td>
                    <td><?= date('d/m/Y', strtotime($t->tanggal_kembali)); ?></td>
                    <td><?= number_format($t->harga, 0, ',', '.'); ?></td>
                    <td><?= number_format($t->denda, 0, ',', '.'); ?></td>
                    <td><?= number_format($t->total_denda, 0, ',', '.'); ?></td>
                    <td><?php if ($t->tanggal_pengembalian == '0000-00-00') {
                            echo '-';
                        } else {
                            echo date('d/m/Y', strtotime($t->tanggal_pengembalian));
                        } ?>
                    </td>
                    <td>
                        <?= $t->status_pengembalian; ?>
                    </td>
                    <td>
                        <?= $t->status_rental; ?>
                    </td>
                    <td>
                        <center>
                            <?php if (empty($t->bukti_pembayaran)) { ?>
                                <button class="btn btn-sm btn-danger"><i class="fas fa-times-circle"></i></button>
                            <?php } else { ?>
                                <a href="<?= base_url('admin/transaksi/pembayaran/' . $t->id_transaksi); ?>" class="btn btn-sm btn-primary"><i class="fas fa-check"></i></a>
                            <?php } ?>
                        </center>
                    </td>
                    <td>
                        <?php if ($t->status == '1') {
                            echo '-';
                        } else { ?>
                            <div class="row">
                                <a href="<?= base_url('admin/transaksi/transaksi_selesai/' . $t->id_transaksi); ?>" class="btn btn-sm btn-success mr-2"><i class="fas fa-check"></i></a>
                                <a href="<?= base_url('admin/transaksi/transaksi_batal/' . $t->id_transaksi); ?>" class="btn btn-sm btn-danger"><i class="fas fa-times"></i></a>
                            </div>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>