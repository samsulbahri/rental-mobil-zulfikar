<div class="container">
    <div class="card mx-auto" style="margin-top: 180px; width:80%;">
        <div class="card-header">
            Data Transaksi Anda
        </div>
        <span class="mt-2 p-2"><?= $this->session->flashdata('pesan'); ?></span>
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <tr>
                    <th>No</th>
                    <th>Nama Customer</th>
                    <th>Merk Mobil</th>
                    <th>No Plat</th>
                    <th>Harga Sewa</th>
                    <th>Action</th>
                    <th>Batal</th>
                </tr>

                <?php $no = 1;
                foreach ($transaksi as $t) { ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $t->nama; ?></td>
                        <td><?= $t->merk; ?></td>
                        <td><?= $t->no_plat; ?></td>
                        <td><?= number_format($t->harga, 0, ',', '.'); ?></td>
                        <td>
                            <?php if ($t->status_rental == 'Selesai') { ?>
                                <button class="btn btn-sm btn-danger">Rental Selesai</button>
                            <?php } else { ?>
                                <a href="<?= base_url('customer/transaksi/pembayaran/' . $t->id_transaksi); ?>" class="btn btn-sm btn-success">Cek Transaksi</a>
                            <?php } ?>
                        </td>
                        <td>
                            <?php
                            if ($t->status_rental == "Belum Selesai") { ?>
                                <a onclick="return confirm('Yakin Batal?')" href="<?= base_url('customer/transaksi/batal_transaksi' . $t->id_transaksi); ?>" class="btn btn-sm btn-danger">Batal</a>
                            <?php } else { ?>
                                <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#exampleModal">
                                    Batal
                                </button>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Informasi Batal Transaksi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Maaf, Transaksi ini telah selesai dan tidak dapat dibatalkan.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>