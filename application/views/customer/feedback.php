<div class="container">
    <div class="card" style="margin-top:200px; margin-bottom:50px;">
        <div class="card-header">
            Form Feedback
        </div>
        <div class="card-body">
            <div class="review-form">
                <?php foreach ($transaksi as $t) { ?>
                    <form action="<?= base_url('customer/feedback/tambah_feedback'); ?>" method="POST">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="hidden" name="id_customer" value="<?= $this->session->userdata('id_customer'); ?>">
                                    <input type="text" placeholder="Full Name" class="form-control" value="<?= $t->nama; ?>" readonly>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label>Mobil</label>
                                    <select name="mobil" class="form-control" required>
                                        <?php
                                        foreach ($transaksi as $tr) {
                                            if ($tr->status_feedback == '0') { ?>
                                                <option value="<?= $tr->id_mobil . '|' . $tr->id_transaksi; ?>"><?= $tr->merk . ' Pada Tanggal ' . $tr->tanggal_rental; ?></option>
                                        <?php }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="message-input">
                            <textarea class="form-control" name="pesan" cols="30" rows="5" placeholder="Tulis Feedback anda disini..."></textarea>
                        </div>
                        <div class="input-submit">
                            <button type="submit">Kirim</button>
                            <button type="reset">Clear</button>
                        </div>
                    </form>
                <?php
                    break;
                } ?>
            </div>
        </div>
    </div>
</div>