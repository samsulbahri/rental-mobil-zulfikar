<table style="width: 60%;">
    <h2>Invoice Pembayaran Anda</h2>
    <?php foreach ($transaksi as $t) { ?>
        <tr>
            <td>Nama Customer</td>
            <td>:</td>
            <td><?= $t->nama; ?></td>
        </tr>
        <tr>
            <td>Merk Mobil</td>
            <td>:</td>
            <td><?= $t->merk; ?></td>
        </tr>

        <tr>
            <td>Tanggal Rental</td>
            <td>:</td>
            <td><?= $t->tanggal_rental; ?></td>
        </tr>

        <tr>
            <td>Tanggal Kembali</td>
            <td>:</td>
            <td><?= $t->tanggal_kembali; ?></td>
        </tr>

        <tr>
            <td>Biaya Sewa/Hari</td>
            <td>:</td>
            <td><?= number_format($t->harga, 0, ',', '.'); ?></td>
        </tr>

        <tr>
            <?php
            $x = strtotime($t->tanggal_kembali);
            $y = strtotime($t->tanggal_rental);

            $jumlahHari = abs(($x - $y) / (60 * 60 * 24));
            ?>
            <td>Jumlah Sewa Hari</td>
            <td>:</td>
            <td><?= $jumlahHari; ?> Hari</td>
        </tr>

        <tr>
            <td>Status Pembayaran</td>
            <td>:</td>
            <td><?php if ($t->status_pembayaran == '0') {
                    echo 'Belum Lunas';
                } else {
                    echo 'Lunas';
                } ?></td>
        </tr>

        <tr style="font-weight: bold; color: red;">
            <td>Jumlah Pembayaran</td>
            <td>:</td>
            <td>Rp. <?= number_format($t->harga * $jumlahHari, 0, ',', '.'); ?></td>
        </tr>

        <tr>
            <td>Rekening Pembayaran</td>
            <td>:</td>
            <td>
                <ul>
                    <li>Bank Mandiri: 971273819</li>
                    <li>Bank BNI: 8973912</li>
                    <li>Bank BCA: 1238129371</li>
                    <li>Bank Kaltimtara: 09876589</li>
                    <li>Bank Jago: 12345</li>
                </ul>
            </td>
        </tr>
    <?php } ?>
</table>

<script>
    window.print();
</script>