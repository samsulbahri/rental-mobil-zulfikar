<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col-md-8">
            <div class="card" style="margin-top:150px;">
                <div class="card-header alert alert-success">
                    Invoice Pembayaran
                </div>

                <div class="card-body">
                    <table class="table">
                        <?php foreach ($transaksi as $t) { ?>
                            <tr>
                                <th>Merk Mobil</th>
                                <td>:</td>
                                <td><?= $t->merk; ?></td>
                            </tr>

                            <tr>
                                <th>Tanggal Rental</th>
                                <td>:</td>
                                <td><?= $t->tanggal_rental; ?></td>
                            </tr>

                            <tr>
                                <th>Tanggal Kembali</th>
                                <td>:</td>
                                <td><?= $t->tanggal_kembali; ?></td>
                            </tr>

                            <tr>
                                <th>Biaya Sewa/Hari</th>
                                <td>:</td>
                                <td><?= number_format($t->harga, 0, ',', '.'); ?></td>
                            </tr>

                            <tr>
                                <?php
                                $x = strtotime($t->tanggal_kembali);
                                $y = strtotime($t->tanggal_rental);

                                $jumlahHari = abs(($x - $y) / (60 * 60 * 24));
                                ?>
                                <th>Jumlah Sewa Hari</th>
                                <td>:</td>
                                <td><?= $jumlahHari; ?> Hari</td>
                            </tr>

                            <tr class="text-success">
                                <th>Jumlah Pembayaran</th>
                                <td>:</td>
                                <td><button class="btn btn-success btn-sm">Rp. <?= number_format($t->harga * $jumlahHari, 0, ',', '.'); ?></button></td>
                            </tr>

                            <tr>
                                <td></td>
                                <td></td>
                                <td><a href="<?= base_url('customer/transaksi/cetak_invoice/' . $t->id_transaksi); ?>" class="btn btn-sm btn-secondary">Print Invoice</a></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card" style="margin-top:150px;">
                <div class="card-header alert alert-primary">
                    Informasi Pembayaran
                </div>

                <div class="card-body">
                    <p class="text-secondary mb-2">Silahkan melakukan pembayaran melalui nomor rekening di bawah ini: </p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Bank Mandiri: 971273819</li>
                        <li class="list-group-item">Bank BNI: 8973912</li>
                        <li class="list-group-item">Bank BCA: 1238129371</li>
                        <li class="list-group-item">Bank Kaltimtara: 09876589</li>
                        <li class="list-group-item">Bank Jago: 12345</li>
                    </ul>

                    <?php if (empty($t->bukti_pembayaran)) { ?>
                        <button type="button" style="width: 100%;" class="btn btn-danger btn-sm mt-3" data-toggle="modal" data-target="#exampleModal">
                            Upload Bukti Pembayaran
                        </button>
                    <?php } else if ($t->status_pembayaran == '0') { ?>
                        <button class="btn btn-sm btn-warning" style="width: 100%;"><i class="fa fa-clock-o"></i> Menunggu Konfirmasi</button>
                    <?php } else if ($t->status_pembayaran == '1') { ?>
                        <button class="btn btn-sm btn-success" style="width: 100%;"><i class="fa fa-check"></i> Pembayaran Selesai</button>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?= base_url('customer/transaksi/pembayaran_aksi'); ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Upload Bukti Pembayaran anda</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Upload Bukti Pembayaran</label>
                        <?php foreach ($transaksi as $t) { ?>
                            <input type="file" name="bukti_pembayaran" class="form-control">
                            <input type="hidden" name="id_transaksi" value="<?= $t->id_transaksi; ?>" class="form-control">
                        <?php } ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-success">Kirim</button>
                </div>
            </form>
        </div>
    </div>
</div>