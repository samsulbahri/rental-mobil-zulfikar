<!--== Page Title Area Start ==-->
<section id="page-title-area" class="section-padding overlay">
    <div class="container">
        <div class="row">
            <!-- Page Title Start -->
            <div class="col-lg-12">
                <div class="section-title  text-center">
                    <h2>Our Cars</h2>
                    <span class="title-line">
                        <i class="fa fa-car"></i>
                    </span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
            </div>
            <!-- Page Title End -->
        </div>
    </div>
</section>
<!--== Page Title Area End ==-->

<!--== Car List Area Start ==-->
<section id="car-list-area" class="section-padding">
    <div class="container">
        <?= $this->session->flashdata('pesan'); ?>
        <div class="row">
            <!-- Car List Content Start -->
            <div class="col-lg-12">
                <div class="car-list-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="filters" class="popucar-menu text-center">
                                <a href="#" data-filter="*" class="active">all</a>
                                <?php foreach ($type as $t) { ?>
                                    <a href="#" data-filter=".<?= $t->kode_type; ?>" class=""><?= $t->nama_type; ?></a>
                                <?php } ?>
                            </div>

                            <div id="sortir" class="popucar-menu text-center">
                                <a href="#" data-sort-by="asc" class="">Termurah - Termahal</a>
                                <a href="#" data-sort-by="desc" class="">Termahal - Termurah</a>
                            </div>
                        </div>
                    </div>
                    <div class="row grid">
                        <?php foreach ($mobil as $m) { ?>
                            <div class="col-lg-6 col-md-6 grid-item <?= $m->kode_type; ?>">
                                <div class="single-car-wrap">
                                    <img src="<?= base_url('assets/upload/' . $m->gambar); ?>">
                                    <div class="car-list-info without-bar">
                                        <h2>
                                            <a href="#"><?= $m->merk; ?></a>
                                        </h2>
                                        <h5 class="price">Rp. <?= number_format($m->harga, 0, ',', '.'); ?> / Hari</h5>
                                        <ul class="car-info-list">
                                            <li><?php if ($m->ac == "1") {
                                                    echo '<i class="fas fa-check-square text-warning"></i>';
                                                } else {
                                                    echo '<i class="fas fa-times-circle text-warning"></i>';
                                                } ?> AC</li>
                                            <li><?php if ($m->supir == "1") {
                                                    echo '<i class="fas fa-check-square text-warning"></i>';
                                                } else {
                                                    echo '<i class="fas fa-times-circle text-warning"></i>';
                                                } ?> Supir</li>
                                            <li><?php if ($m->mp3_player == "1") {
                                                    echo '<i class="fas fa-check-square text-warning"></i>';
                                                } else {
                                                    echo '<i class="fas fa-times-circle text-warning"></i>';
                                                } ?> MP3 Player</li>
                                            <li><?php if ($m->central_lock == "1") {
                                                    echo '<i class="fas fa-check-square text-warning"></i>';
                                                } else {
                                                    echo '<i class="fas fa-times-circle text-warning"></i>';
                                                } ?> Central Lock</li>
                                        </ul>
                                        <p class="rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star unmark"></i>
                                        </p>
                                        <?php if ($m->status == 1) {
                                            echo anchor('customer/rental/tambah_rental/' . $m->id_mobil, '<span class="rent-btn">Rental</span>');
                                        } else {
                                            echo '<span class="rent-btn">Tidak Tersedia</span>';
                                        } ?>
                                        <a href="<?= base_url('customer/data_mobil/detail_mobil/' . $m->id_mobil); ?>" class="rent-btn">Detail</a>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <!-- Car List Content End -->
        </div>
    </div>
</section>
<!--== Car List Area End ==-->

<!--== Footer Area Start ==-->
<section id="footer-area">
    <!-- Footer Widget Start -->
    <div class="footer-widget-area">
        <div class="container">
            <div class="row">
                <!-- Single Footer Widget Start -->
                <div class="col-lg-4 col-md-6">
                    <div class="single-footer-widget">
                        <h2>About Us</h2>
                        <div class="widget-body">
                            <img src="assets/img/logo.png" alt="JSOFT" data-pagespeed-url-hash="2987653198" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                            <p>Lorem ipsum dolored is a sit ameted consectetur adipisicing elit. Nobis magni assumenda distinctio debitis, eum fuga fugiat error reiciendis.</p>
                            <div class="newsletter-area">
                                <form action="index.html">
                                    <input type="email" placeholder="Subscribe Our Newsletter">
                                    <button type="submit" class="newsletter-btn">
                                        <i class="fa fa-send"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Single Footer Widget End -->
                <!-- Single Footer Widget Start -->
                <div class="col-lg-4 col-md-6">
                    <div class="single-footer-widget">
                        <h2>Recent Posts</h2>
                        <div class="widget-body">
                            <ul class="recent-post">
                                <li>
                                    <a href="#">

                                        Hello Bangladesh!
                                        <i class="fa fa-long-arrow-right"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">

                                        Lorem ipsum dolor sit amet
                                        <i class="fa fa-long-arrow-right"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">

                                        Hello Bangladesh!
                                        <i class="fa fa-long-arrow-right"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">

                                        consectetur adipisicing elit?
                                        <i class="fa fa-long-arrow-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Single Footer Widget End -->
                <!-- Single Footer Widget Start -->
                <div class="col-lg-4 col-md-6">
                    <div class="single-footer-widget">
                        <h2>get touch</h2>
                        <div class="widget-body">
                            <p>Lorem ipsum doloer sited amet, consectetur adipisicing elit. nibh auguea, scelerisque sed</p>
                            <ul class="get-touch">
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    800/8, Kazipara, Dhaka
                                </li>
                                <li>
                                    <i class="fa fa-mobile"></i>
                                    +880 01 86 25 72 43
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    kazukamdu83@gmail.com
                                </li>
                            </ul>
                            <a href="https://goo.gl/maps/b5mt45MCaPB2" class="map-show" target="_blank">Show Location</a>
                        </div>
                    </div>
                </div>
                <!-- Single Footer Widget End -->
            </div>
        </div>
    </div>
    <!-- Footer Widget End -->
    <!-- Footer Bottom Start -->
    <div class="footer-bottom-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->

                        Copyright &copy;
                        <script>
                            document.write(new Date().getFullYear());
                        </script>
                        All rights reserved | This template is made with
                        <i class="fa fa-heart-o" aria-hidden="true"></i>
                        by
                        <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer Bottom End -->
</section>
<!--== Footer Area End ==-->
<!--== Scroll Top Area Start ==-->
<div class="scroll-top">
    <img src="assets/img/scroll-top.png" alt="JSOFT" data-pagespeed-url-hash="1765177560" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
</div>
<!--== Scroll Top Area End ==-->

<!-- Isotope -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>

<script>
    var $grid = $('.grid').isotope({
        itemSelector: '.grid-item',
        getSortData: {
            price: function(itemElem) {
                var price = $(itemElem).find('.price').text();
                var finalPrice = parseInt(price.replace(/[Rp./Hari ]/g, ''));
                return finalPrice;
            }
        }
    });

    var filterFns = {
        numberGreaterThan50: function() {
            var number = $(this).find('.number').text();
            return parseInt(number, 10) > 50;
        }
    };

    $("#filters").find("a").click(function() {
        var filterValue = $(this).attr('data-filter');
        filterValue = filterFns[filterValue] || filterValue;
        $grid.isotope({
            filter: filterValue
        });
    });

    $("#sortir").find("a").click(function() {
        var sortByValue = $(this).attr('data-sort-by');
        console.log(sortByValue);
        if (sortByValue == 'desc') {
            $grid.isotope({
                sortBy: 'price',
                sortAscending: false
            });
        } else {
            $grid.isotope({
                sortBy: 'price',
                sortAscending: true
            });
        }
        $grid.isotope({
            sortBy: 'price'
        });
    });
</script>