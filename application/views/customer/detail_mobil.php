<div class="container mt-5 mb-5">
    <div class="card" style="margin-top: 200px;">
        <div class="card-body">
            <?php foreach ($detail as $d) { ?>
                <div class="row">
                    <div class="col-md-6">
                        <img class="img-fluid" src="<?= base_url('assets/upload/' . $d->gambar); ?>">
                    </div>
                    <div class="col-md-6">
                        <table class="table">
                            <tr>
                                <th>Merk</th>
                                <td><?= $d->merk; ?></td>
                            </tr>
                            <tr>
                                <th>Nomor Plat</th>
                                <td><?= $d->no_plat; ?></td>
                            </tr>
                            <tr>
                                <th>Warna</th>
                                <td><?= $d->warna; ?></td>
                            </tr>
                            <tr>
                                <th>Tahun Produksi</th>
                                <td><?= $d->tahun; ?></td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>
                                    <?php if ($d->status == '1') {
                                        echo 'Tersedia';
                                    } else {
                                        echo 'Tidak Tersedia / Sedang Dirental';
                                    } ?>
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <?php
                                    if ($d->status == '0') {
                                        echo '<span class="btn btn-danger" disable>Telah di Rental</span>';
                                    } else {
                                        echo anchor('customer/rental/tambah_rental/' . $d->id_mobil, '<button class="btn btn-success">Rental</button>');
                                    }
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>