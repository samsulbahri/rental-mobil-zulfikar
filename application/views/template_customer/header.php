  <!DOCTYPE html>
  <html class="no-js" lang="zxx">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <title>Cardoor - Car Rental HTML Template</title>
    <!--=== Bootstrap CSS ===-->
    <link href="https://preview.colorlib.com/theme/cardoor/assets/css/A.bootstrap.min.css+plugins,,_vegas.min.css+plugins,,_slicknav.min.css+plugins,,_magnific-popup.css+plugins,,_owl.carousel.min.css+plugins,,_gijgo.css+font-awesome.css,Mcc.jdoNp89zmk.css.pagespeed.cf.0v-DtFlKCq.css" rel="stylesheet" />
    <!--=== Vegas Min CSS ===-->
    <!--=== Slicknav CSS ===-->
    <!--=== Magnific Popup CSS ===-->
    <!--=== Owl Carousel CSS ===-->
    <!--=== Gijgo CSS ===-->
    <!--=== FontAwesome CSS ===-->
    <!--=== Theme Reset CSS ===-->
    <link href="<?= base_url('assets/cardoor/css/style.css'); ?>" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <!--=== Main Style CSS ===-->
    <!--=== Responsive CSS ===-->
    <!--[if lt IE 9]>
            <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
  </head>

  <body class="loader-active">
    <!--== Preloader Area Start ==-->
    <div class="preloader">
      <div class="preloader-spinner">
        <div class="loader-content">
          <img src="https://preview.colorlib.com/theme/cardoor/assets/img/preloader.gif.pagespeed.ce.bl_tqnjOKO.gif" alt="JSOFT" data-pagespeed-url-hash="544742988" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
        </div>
      </div>
    </div>
    <!--== Preloader Area End ==-->