<!--== Header Area Start ==-->
<header id="header-area" class="fixed-top">
  <!--== Header Top Start ==-->
  <div id="header-top" class="d-none d-xl-block">
    <div class="container">
      <div class="row">
        <!--== Single HeaderTop Start ==-->
        <div class="col-lg-3 text-left">
          <i class="fa fa-map-marker"></i>
          802/2, Mirpur, Dhaka
        </div>
        <!--== Single HeaderTop End ==-->
        <!--== Single HeaderTop Start ==-->
        <div class="col-lg-3 text-center">
          <i class="fa fa-mobile"></i>
          +1 800 345 678
        </div>
        <!--== Single HeaderTop End ==-->
        <!--== Single HeaderTop Start ==-->
        <div class="col-lg-3 text-center">
          <i class="fa fa-clock-o"></i>
          Mon-Fri 09.00 - 17.00
        </div>
        <!--== Single HeaderTop End ==-->
        <!--== Social Icons Start ==-->
        <div class="col-lg-3 text-right">
          <div class="header-social-icons">
            <a href="#">
              <i class="fa fa-behance"></i>
            </a>
            <a href="#">
              <i class="fa fa-pinterest"></i>
            </a>
            <a href="#">
              <i class="fa fa-facebook"></i>
            </a>
            <a href="#">
              <i class="fa fa-linkedin"></i>
            </a>
          </div>
        </div>
        <!--== Social Icons End ==-->
      </div>
    </div>
  </div>
  <!--== Header Top End ==-->
  <!--== Header Bottom Start ==-->
  <div id="header-bottom">
    <div class="container">
      <div class="row">
        <!--== Logo Start ==-->
        <div class="col-lg-4">
          <a href="index2.html" class="logo">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAL4AAAAyCAMAAADV2JEMAAABlVBMVEX/0AD/////0AD/0AD/0AD/0AD/0AD/0AD/0AD/0AD/0AD/0AD/0AD/////0AD/0AD/0AD/0AD/0AD/0AD/0AD/0AD/0AD/0AD/////0AD/0AD/0AD/0AD/////0AD/0AD/0AD/0AD/0AD/////0AD/0AD/0AD/0AD/0AD/0AD/////0AD/0AD/0AD/0AD/0AD/0AD/////0AD/0AD/0AD/0AD/0AD/0AD/0AD/////0AD/0AD/0AD/0AD/0AD/0AD/0AD/0AD/////0AD/0AD/0AD/0AD/0AD/0AD/0AD/////0AD/0AD/0AD/0AD/0AD/0AD/0AD/0AD/////0AD/0AD/0AD/////0AD/0AD/0AD/0AD/0AD/0AD/0AD/0AD/0AD/////0AD/0AD/0AD/0AD/0AD/0AD/0AD/0AD/0AD/////0AD/0AD/0AD/0AD/0AD/////0AD/0AD/0AD/0AD/0AD/0AD/0AD/////0AD/0AD/0AD/0AD/0AD/0AD/0AD/0AD/0AD/0AD/0AD/0AD///+WqDFCAAAAhXRSTlMAAAECAwQGCgsMDQ8QEBEVFhcYGhscHh8gIyQsMDAyMzc/QEBESElKTU5QUVVXXF5fYGRmaWpsbm9wcnV2d3t9fn9/gYOEhoeIj4+RkpWWl5mcn5+ipaqvsLGyt7i6vb6/v8DCxMXGx8zNzs/U2dzd39/i4+Xp6+zu7/Dx8/T19vn7/P3+9ahabwAABMhJREFUaN7tmf171EQQx5PjpCpGsIiGl0NKKrYRhcCV83yhNiAvp2IhwAFaMSAIBISAUMN7r5383e7O7iWzSS62PvqYe7jvT8l2svfZ2ZnZl2r6UEsb4Y/wX1X8f6AR/n+E/+HCC8jqxcKe4cCvn4Zina4PA/4pGKRTQ4B/DOBpa2Mu1De2ngJ8U1382uTR+QM1rQnwfFdhru56DtDUagfmj07Wqod/jofHx9M96E0PqDX4t2ludq5y+G9jdF9GDw8Sn5mf0e6tquGvuyfz83hJrT8ube6tq1zw7H6CZPO1EvzaD2jzZHcFK8+2C4zsYr10qa1fZDYXtlWxcI5dA3h55KtyHXkJcH2sgvjo2NUJp0jpwHRcKSdpc/xG8my5qWwDm9IGK+3HsBUbVKONTW3Zm+PmvuT4J2H1Oqnim36cyE8a4zhMLNyYKMIhkobQlvCd1MaVA7DCtGuk9WlXYUPiT6ysAX9lguI3orgAn/+MW4gfx7aKH8ce0ge0KUB+RzFzsvhxZAr8a7AWXSf4BqPvWtl4tJPOJX5i7kQ4LcngTDcSA2JggYgaw2E+73Lfcx+IqGl47LnBrdIJdsXANW0H9L7cOnVlFeRXprZ+0YMdKX5HOk+REbKQEAgZfHwxCT4jwziz+x7vzwRjDYTHZdRjfwSfv4SI/zXM8gTY++hv4B99ws3mYC7FD0mMU0SLu9MqwLewmeALaw9HRYw6PH+oZ1h/hoov+tW0X+FdrJ5bbpbS39yCVpvhtwTfoBwkb32avQq+ncNv8JeAcnGv+NKSut9S8TsS/w94TSxMG26V0N/aIFcv+FMjbsrj+8KTbv9vCr6HL8pn/CVW8Tmmq+JbOfwQXzRtGXbKhXU7wGcfFKkFsF3a7ATQiKPdgrx1ZQaI7EV8S8gTEUE/wxkM8/jOAHxD9GT7IjU0DeBHuV3YtAT7CjcMn8LSJun8nwi+HiUJSvPWMH0mWUAEvloTKb7DS083jmgffJAsqNq6Eiom4ltqxeX4cOPwzMGZ2bOLAN8V4n8LsHh2ltkcvgEU38MakclbTiNlZfE9Q8/gB3Fk8DG0M33wsmDQCh3oKn5bT/BT3V9fQL/+vmKj0TSNrFze9udXZG8a+2yNixoqvtEVzyHhb8cclQ8p6JcjM8ARpbHv9CttBp+VxbzmYAA+LqkB2fP4dDpE9pLUZT+K/GyM4otuJDG4T8MOb+qE0gjX2K4wk6FCUteTH2bxV84czOrMykB87iqyabB5xaYzzrKXVh7JT5d+X0SIFWV3M7rhEbOOruL3+RH/9gmu8nVrEW1uZ/B10wtTkpAHsq6ulUrhFPzptqWbLKyGK7c9fjvpwuqKQYWepWfxJT/ii3qzh3v5wd28HvBZ+Qht9mXx//f9PnP6VHqenSiI/Yn0HDzFNg/Vwr/MzoDilFu/mrlOk/oe4KpYGmrsVPlLtfAPMd+ef19saB5CbyZ75K1/3oOHm/HxvfPM9lC18F+/y5iWRJTzK4fHmch/zC8YxOMSe/x9rGJn3daajiutqh3Vm2vCb1YWv7l3sJqVxd8vwZbfKLmlenNZWu2vGv74MwF2qfSW7ZIwevZO5a6pJu/w2rMwXoo/vsDrzp3J0X8W/018fag1wh/hv6r4fwG25pv4rAZSAAAAAABJRU5ErkJggg==" alt="JSOFT" data-pagespeed-url-hash="2987653198" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
          </a>
        </div>
        <!--== Logo End ==-->
        <!--== Main Menu Start ==-->
        <div class="col-lg-8 d-none d-xl-block">
          <nav class="mainmenu alignright">
            <ul>
              <li>
                <a href="<?= base_url('customer/dashboard'); ?>">Beranda</a>
              </li>
              <li>
                <a href="<?= base_url('customer/data_mobil'); ?>">Mobil</a>
              </li>
              <li>
                <a href="<?= base_url('customer/transaksi'); ?>">Transaksi</a>
              </li>
              <li>
                <a href="<?= base_url('customer/feedback'); ?>">Feedback</a>
              </li>
              <?php if ($this->session->userdata('nama')) { ?>
                <li>
                  <a href="<?= base_url('auth/logout'); ?>">Welcome <?= $this->session->userdata('nama'); ?><span> | Logout</span></a>
                </li>
              <?php } else { ?>
                <li>
                  <a href="<?= base_url('auth/login'); ?>">Login</a>
                </li>

                <li>
                  <a href="<?= base_url('register'); ?>">Register</a>
                </li>
              <?php } ?>
            </ul>
          </nav>
        </div>
        <!--== Main Menu End ==-->
      </div>
    </div>
  </div>
  <!--== Header Bottom End ==-->
</header>
<!--== Header Area End ==-->