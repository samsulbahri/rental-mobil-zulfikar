<!--== Footer Area Start ==-->
<section id="footer-area">
  <!-- Footer Bottom Start -->
  <div class="footer-bottom-area fixed-bottom">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <p>
            Copyright &copy; 2020 Information System By Zulkipli Al-Ghozali
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer Bottom End -->
</section>
<!--== Footer Area End ==-->
<!--== Scroll Top Area Start ==-->
<div class="scroll-top">
  <img src="assets/img/scroll-top.png.pagespeed.ce.CeIuKeUBGA.png" alt="JSOFT" data-pagespeed-url-hash="1765177560" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
</div>
<!--== Scroll Top Area End ==-->

<!--=======================Javascript============================-->
<!--=== Jquery Min Js ===-->
<script src="https://preview.colorlib.com/theme/cardoor/assets/js/jquery-3.2.1.min.js.pagespeed.jm.Y8jX7FH_5H.js"></script>
<!--=== Jquery Migrate Min Js ===-->
<script src="https://preview.colorlib.com/theme/cardoor/assets/js/jquery-migrate.min.js+popper.min.js+bootstrap.min.js.pagespeed.jc.l_KjLfXlV8.js"></script>
<script>
  eval(mod_pagespeed_$xaS8o35Wv);
</script>
<!--=== Popper Min Js ===-->
<script>
  eval(mod_pagespeed_b9u$kTjOHm);
</script>
<!--=== Bootstrap Min Js ===-->
<script>
  eval(mod_pagespeed_TWwj57mRxy);
</script>
<!--=== Gijgo Min Js ===-->
<script src=" https://preview.colorlib.com/theme/cardoor/assets/js/plugins/gijgo.js.pagespeed.jm.6QR_BVl1lH.js"> </script>
<!--===Vegas Min Js===-->
<script src="https://preview.colorlib.com/theme/cardoor/assets/js/plugins/vegas.min.js+isotope.min.js+owl.carousel.min.js.pagespeed.jc.LEpCgjaGuo.js">
</script>
<script>
  eval(mod_pagespeed_nJP3nz7RR$);
</script>
<!--=== Isotope Min Js ===-->
<script>
  eval(mod_pagespeed_lleY02OapO);
</script>
<!--=== Owl Caousel Min Js ===-->
<script>
  eval(mod_pagespeed_D48jz6kWAl);
</script>
<!--=== Waypoint Min Js ===-->
<script src="https://preview.colorlib.com/theme/cardoor/assets/js/plugins/waypoints.min.js+counterup.min.js.pagespeed.jc.NwNALDhkhD.js"></script>
<script>
  eval(mod_pagespeed_29rzCUeKUL);
</script>
<!--=== CounTotop Min Js ===-->
<script>
  eval(mod_pagespeed_mNDwj_RRYx);
</script>
<!--=== YtPlayer Min Js ===-->
<script src="https://preview.colorlib.com/theme/cardoor/assets/js/plugins/mb.YTPlayer.js.pagespeed.jm.nUPAgLC-2D.js"></script>
<!--=== Magnific Popup Min Js ===-->
<script src="https://preview.colorlib.com/theme/cardoor/assets/js/plugins,_magnific-popup.min.js+plugins,_slicknav.min.js+main.js.pagespeed.jc.iA6Tj-9H_J.js"></script>
<script>
  eval(mod_pagespeed_yk7ipgbCh1);
</script>
<!--=== Slicknav Min Js ===-->
<script>
  eval(mod_pagespeed_NVl2tI74wI);
</script>
<!--=== Mian Js ===-->
<script>
  eval(mod_pagespeed_iVk2WcNzT8);
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];

  function gtag() {
    dataLayer.push(arguments);
  }
  gtag('js', new Date());
  gtag('config', 'UA-23581568-13');
</script>
</body>

</html>